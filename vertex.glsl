#version 460

layout(location = 0) out vec2 fragUV;
layout(location = 1) out flat uint fragTextureIndex;

vec2 vertices[3] = vec2[](
    vec2(-0.433015, -0.25),
    vec2(0, 0.5),
    vec2(0.433015, -0.25)
);

void main()
{
    gl_Position = vec4(vertices[gl_VertexIndex], 0, 1);
    fragUV = (vertices[gl_VertexIndex] + 1) / 2;
    fragTextureIndex = 2000000;
}
