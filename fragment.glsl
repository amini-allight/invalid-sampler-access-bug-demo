#version 460

layout(location = 0) in vec2 fragUV;
layout(location = 1) in flat uint fragTextureIndex;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D colorTexture[8];

void main()
{
    outColor = texture(colorTexture[fragTextureIndex], fragUV);
}
