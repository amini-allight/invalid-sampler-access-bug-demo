# Invalid Sampler Array Access Bug Demo

A demonstration of a bug in AMDVLK where accessing an out-of-range member of a `sampler2D` uniform array causes the entire GPU driver to lock up. The exact failure mode varies:

- In other programs I have observed this trigger a full GPU reset observable in `dmesg` as `[drm:amdgpu_cs_ioctl [amdgpu]] *ERROR* Failed to initialize parser -125!`. This demo doesn't seem to do this for reasons I don't understand.
- I have observed this demo cause the system to become unresponsive for a minute or more before the demo program crashes and interactivity is restored.
- I have observed this demo cause the system to become unresponsive indefinitely.

This is an invalid operation but locking up in response to it makes development work very difficult. This bug exists in version 2023.Q3.1 and may exist in earlier versions too.

**WARNING:** Obviously if you run this with AMDVLK **your computer will become unresponsive or crash**. When running under RADV the system very briefly becomes unresponsive before the program crashes with `VK_ERROR_DEVICE_LOST` and the system recovers. Other platforms and runtimes have not been tested.
